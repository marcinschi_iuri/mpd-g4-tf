
package sqlmapper_p1.interfaces;

public interface SqlIterable<T> extends Iterable<T>, AutoCloseable {
    SqlIterable<T> where (String clause);
    int count();
}
