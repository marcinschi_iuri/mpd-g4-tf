package sqlmapper_p1.interfaces;

public interface DataMapper<T> {
    SqlIterable<T> getAll();
    void update(T val);
    void delete(T val);
    void insert(T val);
}
