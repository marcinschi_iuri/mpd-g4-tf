package sqlmapper_p1;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import sqlmapper_p1.Anotations.FieldAnnotation;
import sqlmapper_p1.interfaces.SqlIterable;

public class SqlIterableImpl<T> implements SqlIterable<T> {

    Connection _conection;
    Class _type;
    Map<String, String> _statments;
    String _whereClose = "";
    LinkedList<FieldAnnotation> _fieldAnnotations;
    Map<String,Method> _methodsSetByField;
    
    public SqlIterableImpl(Connection conn, Class type, Map<String, String> statments, LinkedList<FieldAnnotation> field_annotations, Map<String,Method> methodsSetByField){
        _conection = conn;
        _type = type;
        _statments = statments;
        _fieldAnnotations = field_annotations;
        _methodsSetByField = methodsSetByField;
    }
    
    @Override
    public SqlIterable<T> where(String clause) {
        if (_whereClose.isEmpty()){
            _whereClose += " "+clause;
        }
        else{
            _whereClose += " and "+clause;
        }       
       return this;
    }

    @Override
    public int count() {
        int count = 0;
        try {
            PreparedStatement ps = _conection.prepareStatement(statement("count"), 
                    ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            count = new CustomIterator<T>(ps, _type, _fieldAnnotations, _methodsSetByField).count();
        } catch (SQLException ex) {
            Logger.getLogger(SqlIterableImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return count;
    }

    @Override
    public Iterator<T> iterator() {
        
        CustomIterator<T> customIterator = null;
        try {
            PreparedStatement ps = _conection.prepareStatement(statement("select"),
                    ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            customIterator = new CustomIterator(ps,_type, _fieldAnnotations, _methodsSetByField);
        } catch (SQLException ex) {
            Logger.getLogger(SqlIterableImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return customIterator;
    }
    
    private String statement(String type){
        String select = _statments.get(type);
        if (_whereClose.isEmpty()){
            select += ";";
        }
        else{
            select += " where "+_whereClose+";";
        }
        return select;
    }

    @Override
    public void close() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); 
    }
    
    
}
