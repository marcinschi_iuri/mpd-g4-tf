
package sqlmapper_p1;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import sqlmapper_p1.DataModel.OrderDetails;
import sqlmapper_p1.interfaces.DataMapper;
public class App {

    public static void main(String[] args) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String URL = "jdbc:sqlserver://localhost:1443;database=Northwind;integratedSecurity=true";           
            
            Connection conn;
            try {
                conn = DriverManager.getConnection(URL);
                Builder b = new Builder(conn);
                
                DataMapper<OrderDetails> orderDetailsMapper = b.build(OrderDetails.class);
                
                System.out.println("==> Count objects [OrderID = 10249]/[ProductID = 14]");
                int count = orderDetailsMapper.getAll()
                        .where("OrderID = 10249")
                        .where("ProductID = 14")
                        .count();
                
                System.out.println("==> Result Count: "+count);
                
                Iterable<OrderDetails> orderDetails = orderDetailsMapper.getAll()
                        .where("OrderID = 10249")
                        .where("ProductID = 14");
                
                System.out.println("==> Get objects [OrderID = 10249]/[ProductID = 14]");                     
                                
                Iterator<OrderDetails> it_orderDetails = orderDetails.iterator();
                
                OrderDetails dmu = null;
                while(it_orderDetails.hasNext()){
                    dmu = it_orderDetails.next();
                    System.out.println(dmu.toString());
                }
                
                System.out.println("==> Update objects [OrderID = 10249]/[ProductID = 14] with Quantity=1000 and UnitPrice = 99.99:");
                dmu.setQuantity(1000);
                dmu.setUnitPrice(new BigDecimal(99.99));
                orderDetailsMapper.update(dmu);
                
                System.out.println("==> Get Updated objects [OrderID = 10249]/[ProductID = 14]:");
                orderDetails = orderDetailsMapper.getAll()
                        .where("OrderID = 10249")
                        .where("ProductID = 14");
                
                for(OrderDetails od : orderDetails){
                    System.out.println(od.toString());
                }
                          
                System.out.println("==> Deleting objects [OrderID = 10249]/[ProductID = 14]");
                System.out.println(dmu.toString());
                orderDetailsMapper.delete(dmu);
                
                System.out.println("==> Count objects [OrderID = 10249]/[ProductID = 14]");
                System.out.println(dmu.toString());
                count = orderDetailsMapper.getAll()
                        .where("OrderID = 10249")
                        .where("ProductID = 14")
                        .count();
                
                System.out.println("==> Result Count: "+count);
                
                System.out.println("==> Inserting object [OrderID = 10249]/[ProductID = 14]:");
                System.out.println(dmu.toString());
                orderDetailsMapper.insert(dmu);
                
                System.out.println("==> Count objects [OrderID = 10249]/[ProductID = 14]");
                count = orderDetailsMapper.getAll()
                        .where("OrderID = 10249")
                        .where("ProductID = 14")
                        .count();
                
                System.out.println("==> Result Count: "+count);
                
            } catch (SQLException ex) {
                Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
