package sqlmapper_p1;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import sqlmapper_p1.Anotations.FieldAnnotation;
import sqlmapper_p1.interfaces.DataMapper;
import sqlmapper_p1.interfaces.SqlIterable;

public class GDataMapper<T> implements DataMapper<T> {

    Connection _conection;
    Class _type;
    Map<String, String> _statments;
    LinkedList<FieldAnnotation> _fieldAnnotations;
    Map<String, Method> _methodsGetByField;
    Map<String, Method> _methodsSetByField;

    public GDataMapper(
            Connection conn,
            Class type,
            LinkedList<FieldAnnotation> field_annotations,
            Map<String, String> statments,
            Map<String, Method> methodsGetByField,
            Map<String, Method> methodsSetByField) {
        _conection = conn;
        _type = type;
        _fieldAnnotations = field_annotations;
        _statments = statments;
        _methodsGetByField = methodsGetByField;
        _methodsSetByField = methodsSetByField;
    }

    @Override
    public SqlIterable<T> getAll() {
        return new SqlIterableImpl<>(_conection, _type, _statments, _fieldAnnotations, _methodsSetByField);
    }

    @Override
    public void update(T val) {
        try {
            Method m;
            Object fieldValue;
            int wherePosition = 1;
            Map<FieldAnnotation,Object> pk_keys = new HashMap<>();
            
            PreparedStatement ps = _conection.prepareStatement(_statments.get("update"),
                    ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);           
            
            for (FieldAnnotation fan : _fieldAnnotations) {
                wherePosition = wherePosition < fan.position()? fan.position():wherePosition;
                m = _methodsGetByField.get(fan.fieldName());
                fieldValue = m.invoke(val);
                parseObjectType(fan.type(), fan.position(), fieldValue, ps);              
                if(fan.primaryKey()){
                    pk_keys.put(fan, fieldValue);
                }
            }
            
            for(Entry<FieldAnnotation,Object> entry : pk_keys.entrySet()){
                parseObjectType(entry.getKey().type(), ++wherePosition, entry.getValue(), ps);
            }
            
            ps.executeUpdate();

        } catch (SQLException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(GDataMapper.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void delete(T val) {
        try {
            PreparedStatement ps = _conection.prepareStatement(_statments.get("delete"),
                    ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            Method m;
            Object fieldValue;
            int wherePosition = 0;
            
             for (FieldAnnotation fan : _fieldAnnotations) {
                if(fan.primaryKey()){
                    m = _methodsGetByField.get(fan.fieldName());
                    fieldValue = m.invoke(val);
                    parseObjectType(fan.type(), ++wherePosition, fieldValue, ps);               
                }
            }           
            
            ps.execute();

        } catch (SQLException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(GDataMapper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void insert(T val) {

        try {
            PreparedStatement ps = _conection.prepareStatement(_statments.get("insert"),
                    ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            Method m;
            Object fieldValue;
            for (FieldAnnotation fan : _fieldAnnotations) {
                m = _methodsGetByField.get(fan.fieldName());
                fieldValue = m.invoke(val);
                parseObjectType(fan.type(), fan.position(), fieldValue, ps);
            }
            ps.executeUpdate();

        } catch (SQLException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(GDataMapper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void parseObjectType(String type, int position, Object fieldValue, PreparedStatement ps) throws SQLException {

        switch (type) {
            case "int":                
                if(fieldValue != null){
                    ps.setInt(position, ((Integer) fieldValue));
                    break;
                }
                ps.setNull(position, java.sql.Types.INTEGER);                
                break;
            case "Integer":
                if(fieldValue != null){
                    ps.setInt(position, ((Integer) fieldValue));
                    break;
                }
                ps.setNull(position, java.sql.Types.INTEGER);                
                break;
            case "String":
                if(fieldValue != null){
                    ps.setString(position,fieldValue.toString()); 
                    break;
                }
                ps.setNull(position, java.sql.Types.VARCHAR);                
                break;
            case "double":
                if(fieldValue != null){
                    ps.setDouble(position, (Double) fieldValue);
                    break;
                }
                ps.setNull(position, java.sql.Types.DOUBLE);                
                break;
            case "Double":
                if(fieldValue != null){
                    ps.setDouble(position, (Double) fieldValue);
                    break;
                }
                ps.setNull(position, java.sql.Types.DOUBLE);                
                break;
            case "long":                
                if(fieldValue != null){
                    ps.setLong(position, (Long) fieldValue);
                    break;
                }
                ps.setNull(position, java.sql.Types.NUMERIC);                
                break;
            case "Long":
                if(fieldValue != null){
                    ps.setLong(position, (Long) fieldValue);
                    break;
                }
                ps.setNull(position, java.sql.Types.NUMERIC);                
                break;
            case "boolean":                
                if(fieldValue != null){
                    ps.setBoolean(position, (Boolean) fieldValue);
                    break;
                }
                ps.setNull(position, java.sql.Types.BOOLEAN);                
                break;
            case "Boolean":
                if(fieldValue != null){
                    ps.setBoolean(position, (Boolean) fieldValue);
                    break;
                }
                ps.setNull(position, java.sql.Types.BOOLEAN);                
                break;
            case "Float":                
                if(fieldValue != null){
                    ps.setFloat(position, ((Float) fieldValue));
                    break;
                }
                ps.setNull(position, java.sql.Types.FLOAT);                
                break;
            case "float":
                if(fieldValue != null){
                    ps.setFloat(position, ((Float) fieldValue));
                    break;
                }
                ps.setNull(position, java.sql.Types.FLOAT);                
                break;
        }
    }

}
