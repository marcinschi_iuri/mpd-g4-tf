package sqlmapper_p1;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import sqlmapper_p1.Anotations.FieldAnnotation;

public class CustomIterator<T> implements Iterator<T> {
    
    ResultSet _res;
    Class _type;
    LinkedList<FieldAnnotation> _annf;
    PreparedStatement _ps;
    Map<String,Method> _methodsSetByField;
    
    public CustomIterator(PreparedStatement ps, Class type, LinkedList<FieldAnnotation> annf, Map<String,Method> methodsSetByField){
        _res = null;
        _type = type;
        _annf = annf;
        _ps = ps;
        _methodsSetByField = methodsSetByField;
    }

    @Override
    public boolean hasNext() {
        try {
            if(_res == null){
                 _res = _ps.executeQuery();
            }
            if(_res.next()){
                _res.previous();
                return true;
            }                
        } catch (SQLException ex) {
            Logger.getLogger(CustomIterator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public T next() {
        try {
            Object objData;
            Method m;
            Object rowObject = _type.newInstance();  

            if(_res == null){
                 _res = _ps.executeQuery();
            }
            if(_res.next()){ 
                for(FieldAnnotation fan : _annf){                    
                    objData = _res.getObject(fan.fieldName());                    
                    if(objData != null){
                        m = _methodsSetByField.get(fan.fieldName());                                          
                        m.invoke(rowObject, objData);
                    }
                }
                return (T)rowObject;
            }            
            
        }
        catch (SQLException | InstantiationException | IllegalAccessException| SecurityException | IllegalArgumentException | InvocationTargetException ex) {
                Logger.getLogger(CustomIterator.class.getName()).log(Level.SEVERE, null, ex);
        }         
        return null;
    }
    
     public int count() {
         int count = 0;
          try {
            if(_res == null){
                 _res = _ps.executeQuery();
            }
            else{
                this.resset();
            }
            while(_res.next()){
                count = _res.getInt(1);
            }                
        } catch (SQLException ex) {
            Logger.getLogger(CustomIterator.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.resset();
        return count;
     }
     
     public void resset() {
        try {
            if(_res != null)
                _res.beforeFirst();
        } catch (SQLException ex) {
            Logger.getLogger(CustomIterator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
}
