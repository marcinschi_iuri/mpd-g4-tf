package sqlmapper_p1;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import sqlmapper_p1.Anotations.EntityAnnotation;
import sqlmapper_p1.Anotations.FieldAnnotation;
import sqlmapper_p1.Anotations.GetMethodAnnotation;
import sqlmapper_p1.Anotations.SetMethodAnnotation;
import sqlmapper_p1.interfaces.DataMapper;

public class Builder {    
    Connection _conn = null;    
    public Builder(Connection con){
        this._conn = con;
    }    
    public <T> DataMapper<T> build(Class type){    
        
        EntityAnnotation _entity_annotation = (EntityAnnotation)type.getAnnotation(EntityAnnotation.class);
        Map<String,Method> _methodsGetByField = new HashMap<>();
        Map<String,Method> _methodsSetByField = new HashMap<>();
        Map<String,String> _statments = new HashMap<>();
        LinkedList<FieldAnnotation> _field_annotations = new LinkedList<>();
        
        for(Field f : type.getDeclaredFields()){
            f.setAccessible(true);
            if(f.isAnnotationPresent(FieldAnnotation.class)){
                _field_annotations.add(f.getDeclaredAnnotation(FieldAnnotation.class));
            }            
        }
        
        String select = "select ";
        String count = "select count(1) as count from ["+_entity_annotation.tableName()+"]";
        String update = "update ["+_entity_annotation.tableName()+"] set ";        
        String insert = "insert into ["+_entity_annotation.tableName()+"] (";        
        String delete = "delete from ["+_entity_annotation.tableName()+"]";
        String whereClouse= " where ";
        String valuesToInsert = "values (";
        String fieldName;
        
        for(FieldAnnotation fann : _field_annotations){
            fieldName =fann.fieldName();
            if(fann.primaryKey()){
                if(whereClouse.equals(" where "))
                    whereClouse += "["+fieldName+"] = ?";
                else
                    whereClouse += " and ["+fieldName+"] = ?";
            }
            select += "["+fieldName+"], ";
            update += "["+fieldName+"] = ?, ";
            insert += "["+fieldName+"], ";         
            valuesToInsert += "?, ";
        }
        
        for(Method m : type.getDeclaredMethods()){
           if(m.isAnnotationPresent(GetMethodAnnotation.class)){
               _methodsGetByField.put(m.getDeclaredAnnotation(GetMethodAnnotation.class).fieldName(), m);
           }
           else if(m.isAnnotationPresent(SetMethodAnnotation.class)){
               _methodsSetByField.put(m.getDeclaredAnnotation(SetMethodAnnotation.class).fieldName(), m);
           }
        }
        
        select = select.substring(0,select.length()-2)+" from ["+_entity_annotation.tableName()+"]";        
        update = update.substring(0,update.length()-2)+whereClouse+";";
        delete += whereClouse+";";
        insert = insert.substring(0,insert.length()-2)+") "
                +valuesToInsert.substring(0,valuesToInsert.length()-2)+");";
        
        
        _statments.put("select", select);        
        _statments.put("update", update);
        _statments.put("insert", insert);
        _statments.put("delete", delete);
        _statments.put("count", count);
        
        System.out.println(_statments.get("count"));
        System.out.println(_statments.get("select"));
        System.out.println(_statments.get("insert"));
        System.out.println(_statments.get("update"));
        System.out.println(_statments.get("delete"));
        System.out.println("===================================");
        System.out.println("===================================");
        System.out.println("===================================");
        
                
        return new GDataMapper(_conn, type, _field_annotations, _statments, _methodsGetByField, _methodsSetByField);
    }
}
