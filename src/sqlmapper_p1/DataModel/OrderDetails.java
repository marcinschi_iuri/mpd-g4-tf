package sqlmapper_p1.DataModel;

import java.math.BigDecimal;
import sqlmapper_p1.Anotations.EntityAnnotation;
import sqlmapper_p1.Anotations.FieldAnnotation;
import sqlmapper_p1.Anotations.GetMethodAnnotation;
import sqlmapper_p1.Anotations.SetMethodAnnotation;



@EntityAnnotation(
        tableName = "Order Details", 
        tableColumns = {
            "OrderID","ProductID","UnitPrice",
            "Quantity","Discount"
        }
)
public class OrderDetails {
    
    @FieldAnnotation(fieldName = "OrderID", type= "int", position = 1, primaryKey = true, foreignKey = true, referenceTable = "Orders")
    private int OrderID;
    @FieldAnnotation(fieldName = "ProductID", type= "int", position = 2, primaryKey = true, foreignKey = true, referenceTable = "Products")
    private int ProductID;
    @FieldAnnotation(fieldName = "UnitPrice", type= "double", position = 3, primaryKey = false, foreignKey = false, referenceTable = "")
    private double UnitPrice;
    @FieldAnnotation(fieldName = "Quantity", type= "int", position = 4, primaryKey = false, foreignKey = false, referenceTable = "")
    private int Quantity;
    @FieldAnnotation(fieldName = "Discount", type= "float", position = 5, primaryKey = false, foreignKey = false, referenceTable = "")
    private float Discount;
    
    
    @GetMethodAnnotation(methodName = "getOrderID", fieldName="OrderID")
    public int getOrderID(){ return this.OrderID;}
    
    @GetMethodAnnotation(methodName = "getProductID", fieldName="ProductID")
    public int getOProductID(){ return this.ProductID;}
    
    @GetMethodAnnotation(methodName = "getUnitPrice", fieldName="UnitPrice")
    public double getUnitPrice(){ return this.UnitPrice;}
    
    @GetMethodAnnotation(methodName = "getQuantity", fieldName="Quantity")
    public int getQuantity(){ return this.Quantity;}
    
    @GetMethodAnnotation(methodName = "getDiscount", fieldName="Discount")
    public float getDiscount(){ return this.Discount;}
    
    
    
    @SetMethodAnnotation(methodName = "setOrderID", fieldName="OrderID")
    public void setOrderID(int OrderID){ this.OrderID = OrderID;}
    
    @SetMethodAnnotation(methodName = "setProductID", fieldName="ProductID")
    public void setProductID(int ProductID){ this.ProductID = ProductID;}
    
    @SetMethodAnnotation(methodName = "setUnitPrice", fieldName="UnitPrice")
    public void setUnitPrice(BigDecimal UnitPrice){ 
            this.UnitPrice = UnitPrice.doubleValue();
    }
    
    @SetMethodAnnotation(methodName = "setQuantity", fieldName="Quantity")
    public void setQuantity(int Quantity){ this.Quantity = Quantity;}
    
    @SetMethodAnnotation(methodName = "setDiscount", fieldName="Discount")
    public void setDiscount(float Discount){ this.Discount = Discount;}
    
    @Override
    public String toString() {
        return "OrderDetails{" + "OrderID=" + OrderID + ", ProductID=" + ProductID + ", unitPrice=" + UnitPrice + ", Quantity=" + Quantity + ", Discount="+Discount+"}";
    }
    
    
}
