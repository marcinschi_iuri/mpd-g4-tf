package sqlmapper_p1.DataModel;

import sqlmapper_p1.Anotations.EntityAnnotation;
import sqlmapper_p1.Anotations.FieldAnnotation;
import sqlmapper_p1.Anotations.GetMethodAnnotation;
import sqlmapper_p1.Anotations.SetMethodAnnotation;


@EntityAnnotation(
        tableName = "Customers", 
        tableColumns = {
            "CustomerID","CompanyName","ContactName",
            "ContactTitle","Address","City","Region",
            "PostalCode","Country","Phone","Fax"
        }
)
public class Customers {
    
    @FieldAnnotation(fieldName = "CustomerID", type= "String", position = 1, primaryKey = true, foreignKey = false, referenceTable = "")
    private String CustomerID;
    @FieldAnnotation(fieldName = "CompanyName", type= "String", position = 2, primaryKey = false, foreignKey = false, referenceTable = "")
    private String CompanyName;
    @FieldAnnotation(fieldName = "ContactName", type= "String", position = 3, primaryKey = false, foreignKey = false, referenceTable = "")
    private String ContactName;
    @FieldAnnotation(fieldName = "ContactTitle", type= "String", position = 4, primaryKey = false, foreignKey = false, referenceTable = "")
    private String ContactTitle;
    @FieldAnnotation(fieldName = "Address", type= "String", position = 5, primaryKey = false, foreignKey = false, referenceTable = "")
    private String Address;
    @FieldAnnotation(fieldName = "City", type= "String", position = 6, primaryKey = false, foreignKey = false, referenceTable = "")
    private String City;
    @FieldAnnotation(fieldName = "Region", type= "String", position = 7, primaryKey = false, foreignKey = false, referenceTable = "")
    private String Region;
    @FieldAnnotation(fieldName = "PostalCode", type= "String", position = 8, primaryKey = false, foreignKey = false, referenceTable = "")
    private String PostalCode;
    @FieldAnnotation(fieldName = "Country", type= "String", position = 9, primaryKey = false, foreignKey = false, referenceTable = "")
    private String Country;
    @FieldAnnotation(fieldName = "Phone", type= "String", position = 10, primaryKey = false, foreignKey = false, referenceTable = "")
    private String Phone;
    @FieldAnnotation(fieldName = "Fax", type= "String", position = 11, primaryKey = false, foreignKey = false, referenceTable = "")
    private String Fax;
    
    
    @GetMethodAnnotation(methodName = "getCustomerID", fieldName="CustomerID")
    public String getCustomerID(){ return this.CustomerID;}
    
    @GetMethodAnnotation(methodName = "getCompanyName", fieldName="CompanyName")
    public String getCompanyName(){return this.CompanyName;}
    
    @GetMethodAnnotation(methodName = "getContactName", fieldName="ContactName")
    public String getContactName(){return this.ContactName;}
    
    @GetMethodAnnotation(methodName = "getContactTitle", fieldName="ContactTitle")
    public String getContactTitle(){return this.ContactTitle;}
    
    @GetMethodAnnotation(methodName = "getAddress", fieldName="Address")
    public String getAddress(){return this.Address;}
    
    @GetMethodAnnotation(methodName = "getCity", fieldName="City")
    public String getCity(){return this.City;}
    
    @GetMethodAnnotation(methodName = "getRegion", fieldName="Region")
    public String getRegion(){return this.Region;}
    
    @GetMethodAnnotation(methodName = "getPostalCode", fieldName="PostalCode")
    public String getPostalCode(){return this.PostalCode;}
    
    @GetMethodAnnotation(methodName = "getCountry", fieldName="Country")
    public String getCountry(){return this.Country;}
    
    @GetMethodAnnotation(methodName = "getPhone", fieldName="Phone")
    public String getPhone(){return this.Phone;}
    
    @GetMethodAnnotation(methodName = "getFax", fieldName="Fax")
    public String getFax(){return this.Fax;}
        
    @SetMethodAnnotation(methodName = "setCustomerID", fieldName="CustomerID")
    public void setCustomerID(String CustomerID){ this.CustomerID = CustomerID;}
    
    @SetMethodAnnotation(methodName = "setCompanyName", fieldName="CompanyName")
    public void setCompanyName(String CompanyName){ this.CompanyName = CompanyName;}
    
    @SetMethodAnnotation(methodName = "setContactName", fieldName="ContactName")
    public void setContactName(String ContactName){ this.ContactName = ContactName;}
    
    @SetMethodAnnotation(methodName = "setContactTitle", fieldName="ContactTitle")
    public void setContactTitle(String ContactTitle){ this.ContactTitle = ContactTitle;}
    
    @SetMethodAnnotation(methodName = "setAddress", fieldName="Address")
    public void setAddress(String Address){ this.Address = Address;}
    
    @SetMethodAnnotation(methodName = "setCity", fieldName="City")
    public void setCity(String City){ this.City = City;}
    
    @SetMethodAnnotation(methodName = "setRegion", fieldName="Region")
    public void setRegion(String Region){ this.Region = Region;}
    
    @SetMethodAnnotation(methodName = "setPostalCode", fieldName="PostalCode")
    public void setPostalCode(String PostalCode){ this.PostalCode = PostalCode;}
    
    @SetMethodAnnotation(methodName = "setCountry", fieldName="Country")
    public void setCountry(String Country){ this.Country = Country;}
    
    @SetMethodAnnotation(methodName = "setPhone", fieldName="Phone")
    public void setPhone(String Phone){ this.Phone = Phone;}
    
    @SetMethodAnnotation(methodName = "setFax", fieldName="Fax")
    public void setFax(String Fax){ this.Fax = Fax;}   
    
}
