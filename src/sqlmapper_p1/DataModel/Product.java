/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sqlmapper_p1.DataModel;

import sqlmapper_p1.Anotations.GetMethodAnnotation;
import sqlmapper_p1.Anotations.SetMethodAnnotation;

/**
 *
 * @author Iurie
 */
public class Product {
    
    private int ProductID;
    private String ProductName;
    private int SupplierID;
    private int CategoryID;
    private String QuantityPerUnit;
    private double UnitPrice;
    private int UnitsInStock;
    private int UnitsOnOrder;
    private int ReorderLevel;
    private boolean Discontinued;
    
    
    @GetMethodAnnotation(methodName = "getProductID", fieldName="ProductID")
    public int getProductID(){ return this.ProductID;}    
    @GetMethodAnnotation(methodName = "getProductName", fieldName="ProductName")
    public String getProductName(){ return this.ProductName;}
    @GetMethodAnnotation(methodName = "getSupplierID", fieldName="SupplierID")
    public int getSupplierID(){ return this.SupplierID;}  
    
    @SetMethodAnnotation(methodName = "setProductID", fieldName="ProductID")
    public void setProductID(int ProductID){ this.ProductID = ProductID;}
    @SetMethodAnnotation(methodName = "setProductName", fieldName="ProductName")
    public void setProductName(String ProductName){ this.ProductName = ProductName;}
    @SetMethodAnnotation(methodName = "setSupplierID", fieldName="SupplierID")
    public void setSupplierID(int SupplierID){ this.SupplierID = SupplierID;}
    
}
